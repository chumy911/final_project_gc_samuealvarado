﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccesShadder : MonoBehaviour {

    [SerializeField] private MeshRenderer player;
    // Use this for initialization
    private float changeColor;


    void Start () {
        print(player.material.GetFloat("Vector1_2ECB63D5"));
	
	}

    // Update is called once per frame
    void Update()
    {
        changeColor += 0.01f;
        player.material.SetFloat("Vector1_2ECB63D5", changeColor);
    }
}
